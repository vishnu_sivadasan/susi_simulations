import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D

os.system("sed -i 's/\t/,/g' x_deviation.txt")
os.system("sed -i 's/\t/,/g' z_deviation.txt")
os.system("sed -i 's/\t/,/g' displ_deviation.txt")

data = pd.read_csv('displ_deviation.txt')
stp_list = np.unique(data.Stp)
Stp = []
mean = []
std = []
for stp in np.sort(stp_list):
  idx = np.where(data.Stp == stp)[0]
  [x, y] = zip(*sorted(zip(data.rhor[idx], data.deviation[idx]), key=lambda x: x[0]))
  print stp, np.mean(y),np.std(y)
  Stp.append(stp)
  mean.append(np.mean(y))
  std.append(np.std(y))
 # print "std =", np.std(y)
  #plt.plot(x,y,'ko')
  #plt.show()

mean = np.array(mean)
stp = np.array(stp)
std = np.array(std)
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set_xscale('log')
line1 = ax.errorbar(Stp, mean, std, linestyle='None', marker='^')
line2 = ax.plot(Stp, mean,'r')
plt.axvline(1.0,color='k', linestyle='--')
#plt.legend(handler_map={line1: HandlerLine2D(numpoints=4)},loc='upper left')
plt.xlabel("Stokes number")
plt.ylabel("parameterised Deviation")
plt.title("parameterised Deviation " +" vs stokes number")
plt.savefig('parameterised Deviation_vs_StokesNumber', dpi=200)

plt.show()
