#!/usr/bin/python 
import os
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt 
from matplotlib.legend_handler import HandlerLine2D

os.system("sed -i 's/\t/,/g' x_deviation.txt")
os.system("sed -i 's/\t/,/g' z_deviation.txt")
os.system("sed -i 's/\t/,/g' displ_deviation.txt")

class analysis:
    def __init__(self, data_path, data_details):
        self.data_str = data_details
        self.data = pd.read_csv(data_path)
        self.path = self.data.path
        self.sep_ratio = self.data.sep_ratio
        self.M = self.data.rhor
        self.Stp = self.data.Stp
        self.dev = self.data.deviation
    def plot_dev_errorbar_vs_stp(self):
        deviation = self.dev
        stp_list = np.unique(self.data.Stp)
        mean= []
        std = []
        Stp = []
        for stp in np.sort(stp_list):
            idx = np.where(self.data.Stp == stp)[0]
            [x, y] = zip(*sorted(zip(self.Stp[idx], deviation[idx]), key=lambda x: x[0]))
#            print stp, np.mean(y),np.std(y)
            Stp.append(stp)
            mean.append(np.mean(y))
            std.append(np.std(y))
        mean = np.array(mean)
        stp = np.array(stp)
        std = np.array(std)
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        fig.set_size_inches(15.5, 7.5)
        ax.set_xscale('log')
        line1 = ax.errorbar(Stp, mean, std, linestyle='None', marker='^')
        line2 = ax.plot(Stp, mean,'r')
        plt.axvline(1.0,color='k', linestyle='--')
        #plt.legend(handler_map={line1: HandlerLine2D(numpoints=4)},loc='upper left')
        plt.xlabel("Stokes number")
        plt.ylabel("parameterised deviation")
        plt.title("mean of Parameterised deviation vs stokes number for "+self.data_str)
        plt.savefig('mean_deviation_vs_StokesNumber_'+self.data_str, dpi=200)
#        print "Generated mean of deviation_vs_stp plot for " + self.data_str
    def plot_dev_vs_stp(self):
        fig = plt.figure()
        fig.set_size_inches(15.5, 7.5)
        ax = fig.add_subplot(1,1,1)
        idx = np.where(self.sep_ratio == 0.25)[0]
        [x, y] = zip(*sorted(zip(self.Stp[idx], self.dev[idx]), key=lambda x: x[0]))
        line1, = ax.plot(x,y,'-o',label="initial sepatation = 0.25")
        idx = np.where(self.sep_ratio == 0.5)[0]
        [x, y] = zip(*sorted(zip(self.Stp[idx], self.dev[idx]), key=lambda x: x[0]))
        line2, = ax.plot(x,y,'-o',label="initial sepatation = 0.50")
        idx = np.where(self.sep_ratio == 0.75)[0]
        [x, y] = zip(*sorted(zip(self.Stp[idx], self.dev[idx]), key=lambda x: x[0]))
        line3, = ax.plot(x,y,'-o',label="initial sepatation = 0.75")
        idx = np.where(self.sep_ratio == 1.0)[0]
        [x, y] = zip(*sorted(zip(self.Stp[idx], self.dev[idx]), key=lambda x: x[0]))
        line4, = ax.plot(x,y,'-o',label="initial sepatation = 1.0")
        ax.set_xscale('log')
        plt.axvline(1.0,color='k', linestyle='--')
        plt.legend(handler_map={line1: HandlerLine2D(numpoints=4)},loc='upper left')
        plt.xlabel("Stokes number")
        plt.ylabel("parameterised deviation")
        plt.title("Parameterised deviation vs stokes number for "+self.data_str)
        plt.savefig('deviation_vs_StokesNumber_'+self.data_str, dpi=200)
        #        plt.show()
    def deviation_wrt_M_vs_Stp(self,M_val=[1,10,100,1000,10000]):
        for m in M_val:
            try:	    
                fig = plt.figure()
                fig.set_size_inches(15.5, 5.5)
                ax = fig.add_subplot(1,1,1)
                idx = np.where((self.M == m)&(self.sep_ratio == 0.25))[0]
                [x, y] = zip(*sorted(zip(self.Stp[idx], self.dev[idx]), key=lambda x: x[0]))
                line1, = ax.plot(x,y,'-o',label="initial sepatation = 0.25")
                idx = np.where((self.M == m)&(self.sep_ratio == 0.5))[0]
                [x, y] = zip(*sorted(zip(self.Stp[idx], self.dev[idx]), key=lambda x: x[0]))
                line1, = ax.plot(x,y,'-o',label="initial sepatation = 0.5")
                idx = np.where((self.M == m)&(self.sep_ratio == 0.75))[0]
                [x, y] = zip(*sorted(zip(self.Stp[idx], self.dev[idx]), key=lambda x: x[0]))
                line1, = ax.plot(x,y,'-o',label="initial sepatation = 0.75")
                idx = np.where((self.M == m)&(self.sep_ratio == 1.0))[0]
                [x, y] = zip(*sorted(zip(self.Stp[idx], self.dev[idx]), key=lambda x: x[0]))
                line1, = ax.plot(x,y,'-o',label="initial sepatation = 1.0")
                ax.set_xscale('log')
                plt.axvline(1.0,color='k', linestyle='--')
                plt.legend(handler_map={line1: HandlerLine2D(numpoints=4)},loc='center left')
                plt.xlabel("Stokes number")
                plt.ylabel("parameterised deviation")
                plt.title("Parameterised deviation vs stokes number with M = "+str(m)+" for "+self.data_str)
                plt.savefig('deviation_vs_StokesNumber_M_'+str(m)+"_"+self.data_str, dpi=200)
                #           plt.show()
            except:
                continue
    def deviation_wrt_sep_vs_Stp(self,M_val=[1,10,100,1000,10000]):
        unique_stp = np.unique(self.data.Stp)
        for i,stp in enumerate(unique_stp):
            fig = plt.figure()
            fig.set_size_inches(15.5, 5.5)
            ax = fig.add_subplot(1,1,1)
            for m in M_val:
                try:
                    idx = np.where((self.M == m)&(self.Stp == stp ))[0]
                    print idx
                    [x, y] = zip(*sorted(zip(self.sep_ratio[idx], self.dev[idx]), key=lambda x: x[0]))
                    line1, = ax.plot(x,y,'-o',label="M="+str(m))
                except:
                    continue
            plt.legend(handler_map={line1: HandlerLine2D(numpoints=5)},loc='center left')
            plt.xlabel("Stokes number")
            plt.ylabel("parameterised deviation")
            plt.title("Parameterised deviation vs separation with Stp= "+str(stp)+" for "+self.data_str)
            plt.savefig('deviation_vs_init_separation'+"_for_Stp_"+str(i)+"_"+self.data_str, dpi=200)

x_analysis = analysis("x_deviation.txt","x_deviation") 
x_analysis.deviation_wrt_M_vs_Stp() 
x_analysis.plot_dev_vs_stp() 
x_analysis.plot_dev_errorbar_vs_stp()
x_analysis.deviation_wrt_sep_vs_Stp()

z_analysis = analysis("z_deviation.txt", "z_deviation") 
z_analysis.deviation_wrt_M_vs_Stp() 
z_analysis.plot_dev_vs_stp() 
z_analysis.plot_dev_errorbar_vs_stp()
z_analysis.deviation_wrt_sep_vs_Stp()

total_analysis = analysis("displ_deviation.txt", "total_devitaion") 
total_analysis.deviation_wrt_M_vs_Stp() 
total_analysis.plot_dev_vs_stp()
total_analysis.plot_dev_errorbar_vs_stp()
total_analysis.deviation_wrt_sep_vs_Stp()