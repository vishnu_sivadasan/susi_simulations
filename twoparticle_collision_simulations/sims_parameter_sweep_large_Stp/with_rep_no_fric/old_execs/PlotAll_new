#!/usr/bin/python
""" Paste to a folder containing the simulation folders to generate a 
    plots folder with all plots.""" 
import os 
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D
import json,os,math
import susireader as susi
import math
#==============================================================================
#Function to load json files
def LoadJSON(filename):
   file = open(filename)
   string = file.read()
   file.close()
   return json.loads(string)
#-------------------------------------------------------------------------------
#funtion to get the list of folders in the current working directory
def get_folder_list():
   list = next(os.walk('.'))[1]
   return list

#-------------------------------------------------------------------------------
#function to get the properties of the shear flow
def get_props(path):
    config_file = path + '/config.json' 
    part_file = path + '/part_1.json'
    fluid_file = path + '/fluid_1.json'
    config = LoadJSON(config_file)
    part = LoadJSON(part_file)
    fluid = LoadJSON(fluid_file)
    yd = config['experiment']['shearrate']['constant']['yd']
    rho_p = part['density']['constant']['rho']
    rho_f = fluid['density']['constant']['rho']
    nu = fluid['viscshear']['constant']['nu']
    distz = config['experiment']['twopartcoll']['distz']
    pdiam = part['size']['mono']['diam']
    friction = part['friction']['type']
    repulsion_c0 = part['repulsion']['simple']['c0']
    repulsion_dmax = part['repulsion']['simple']['dmax']
    #calculating the properties
    Rep = pdiam**2 * yd/nu
    M = rho_p/rho_f
    Stp = Rep*M
    Sep_Ratio = distz/pdiam
    return path,round(Sep_Ratio,2), yd, M, "{:.2e}".format(Rep),"{:.2e}".format(Stp),friction,"{:.2e}".format(repulsion_c0),"{:.2e}".format(repulsion_dmax)

def get_pdiam(path):
    part_file = path + '/part_1.json'
    part = LoadJSON(part_file)
    pdiam = part['size']['mono']['diam']
    return pdiam

def dist_ij(x1,y1,x2,y2):
	n = len(x1)
	rij = np.zeros_like(x1)
	for i in range(n):
		r2ij = (x1[i] -x2[i])**2 +(y1[i] - y2[i])**2
		rij[i] = math.sqrt(r2ij)
	return rij	
#-------------------------------------------------------------------------------
def plot_folder(path):
    if os.path.isfile(path+'/result/traject0.dat') and os.path.isfile(path+'/result/traject1.dat'):
        path,Sep_Ratio, yd, M, Rep,Stp,friction,repulsion_c0,repulsion_dmax= get_props(path)
	pdiam = get_pdiam(path)
	x1,y1,z1 = np.loadtxt(path+'/result/traject0.dat', unpack = True)
        x2,y2,z2 = np.loadtxt(path+'/result/traject1.dat', unpack = True)
	#mean_y = (y2[0] + y1[0])*0.5
	#y1,y2 = y1 - mean_y, y2- mean_y
	exp = susi.DataSet(path,fluid=False,particles=True)
	tlist = exp.tlist[1:]
	rij = dist_ij(x1,z1,x2,z2)
        plt.figure(figsize=(10, 8), dpi=120)
#        plt.plot(x1,z1,'r',x2,z2,'b')
        line1, = plt.plot(tlist,rij,'r', label='Particle separation')
	line2 = plt.axhline(y=pdiam, label='minimum particle separation')
        #line2, = plt.plot(x2,z2,'b', label='Particle 2')    
        plt.legend(handler_map={line1: HandlerLine2D(numpoints=4)})    
        plt.xlabel('rij(m)')
        plt.ylabel('t/t_max')
        path,Sep_Ratio, yd, M, Rep,Stp,friction,repulsion_c0,repulsion_dmax = get_props(path)
        plt.title(path)
        props_info = 'Sep_ratio='+str(Sep_Ratio)+',yd='+ str(yd)+',M='+ str(M)+'\nRep='+ str(Rep)+',Stp='+str(Stp)+',Friction='+str(friction)+',Repulsion_c0='+str(repulsion_c0)+',Repulsion_dmax='+str(repulsion_dmax)
        #plt.gca().set_position((.1, .1, .8, 0.6))
        plt.figtext(.02,.02,str(props_info))
        plt.savefig(path+'particle_dist_Stp_'+str(Stp)+'.png')
        plt.clf()	
        plt.close()
#-------------------------------------------------------------------------------

folders = get_folder_list()
for folder in folders:
	print folder 	
	try:
		plot_folder(folder)
	except:
		continue
#-------------------------------------------------------------------------------
#moving all images to plots folder
if not os.path.exists('./plots'): os.mkdir('plots')
os.system('mv *.png plots/')
