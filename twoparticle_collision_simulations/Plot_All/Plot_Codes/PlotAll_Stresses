#!/usr/bin/python
import os
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt 
from matplotlib.legend_handler import HandlerLine2D

#os.system("sed -i 's/\t/,/g' normal_stress_info.txt")

class analysis:
    def __init__(self, data_path, data_details):
        self.data_str = data_details
        self.data = pd.read_csv(data_path)
        self.path = self.data.path
        self.sep_ratio = self.data.sep_ratio
        self.M = self.data.rhor
        self.Stp = self.data.Stp
    def plot_shear_errorbar_vs_stp(self,stress_type,stress_name):
        if stress_type == 'snorm_tot': 
            stress_type = self.data.snorm_tot
        elif stress_type == 'snorm_hd': 
            stress_type = self.data.snorm_hd
        elif stress_type == 'snorm_lub': 
            stress_type = self.data.snorm_lub
        elif stress_type == 'snorm_rep': 
            stress_type = self.data.snorm_rep
        elif stress_type == 'sshear_hd': 
            stress_type = self.data.sshear_hd
        elif stress_type == 'sshear_lub': 
            stress_type = self.data.sshear_lub
        elif stress_type == 'sshear_rep': 
            stress_type = self.data.sshear_rep
        elif stress_type == 'sshear_tot': 
            stress_type = self.data.sshear_tot
        elif stress_type == 'f_hd': 
            stress_type = self.data.F_hd
        elif stress_type == 'f_lub': 
            stress_type = self.data.F_lub
        elif stress_type == 'f_rep': 
            stress_type = self.data.F_rep
        elif stress_type == 'f_tot': 
            stress_type = self.data.F_tot
        else:
            print "enter valid shear/strss name."
        
        stp_list = np.unique(self.data.Stp)
        mean= []
        std = []
        Stp = []
        for stp in np.sort(stp_list):
            idx = np.where(self.data.Stp == stp)[0]
            [x, y] = zip(*sorted(zip(self.Stp[idx], stress_type[idx]), key=lambda x: x[0]))
#            print stp, np.mean(y),np.std(y)
            Stp.append(stp)
            mean.append(np.mean(y))
            std.append(np.std(y))
        mean = np.array(mean)
        stp = np.array(stp)
        std = np.array(std)
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        fig.set_size_inches(15.5, 7.5)
        ax.set_xscale('log')
        line1 = ax.errorbar(Stp, mean, std, linestyle='None', marker='^')
        line2 = ax.plot(Stp, mean,'r')
        plt.axvline(1.0,color='k', linestyle='--')
        #plt.legend(handler_map={line1: HandlerLine2D(numpoints=4)},loc='upper left')
        plt.xlabel("Stokes number")
        plt.ylabel(stress_name)
        plt.title(stress_name +" vs stokes number for "+self.data_str)
        plt.savefig(stress_name+'_vs_StokesNumber_'+self.data_str, dpi=200)
        print "Generated shear_vs_stp plot for " + self.data_str
                
    def plot_shear_vs_stp(self,stress_type,stress_name):
        if stress_type == 'snorm_tot': 
            stress_type = self.data.snorm_tot
        elif stress_type == 'snorm_hd': 
            stress_type = self.data.snorm_hd
        elif stress_type == 'snorm_lub': 
            stress_type = self.data.snorm_lub
        elif stress_type == 'snorm_rep': 
            stress_type = self.data.snorm_rep
        elif stress_type == 'sshear_hd': 
            stress_type = self.data.sshear_hd
        elif stress_type == 'sshear_lub': 
            stress_type = self.data.sshear_lub
        elif stress_type == 'sshear_rep': 
            stress_type = self.data.sshear_rep
        elif stress_type == 'sshear_tot': 
            stress_type = self.data.sshear_tot
        elif stress_type == 'f_hd': 
            stress_type = self.data.F_hd
        elif stress_type == 'f_lub': 
            stress_type = self.data.F_lub
        elif stress_type == 'f_rep': 
            stress_type = self.data.F_rep
        elif stress_type == 'f_tot': 
            stress_type = self.data.F_tot
        else:
            print "enter valid shear/strss name."
            
        fig = plt.figure()
        fig.set_size_inches(15.5, 7.5)
        ax = fig.add_subplot(1,1,1)
        idx = np.where(self.sep_ratio == 0.25)[0]
        [x, y] = zip(*sorted(zip(self.Stp[idx], stress_type[idx]), key=lambda x: x[0]))
        line1, = ax.plot(x,y,'-o',label="initial sepatation = 0.25")
        idx = np.where(self.sep_ratio == 0.5)[0]
        [x, y] = zip(*sorted(zip(self.Stp[idx], stress_type[idx]), key=lambda x: x[0]))
        line2, = ax.plot(x,y,'-o',label="initial sepatation = 0.50")
        idx = np.where(self.sep_ratio == 0.75)[0]
        [x, y] = zip(*sorted(zip(self.Stp[idx], stress_type[idx]), key=lambda x: x[0]))
        line3, = ax.plot(x,y,'-o',label="initial sepatation = 0.75")
        idx = np.where(self.sep_ratio == 1.0)[0]
        [x, y] = zip(*sorted(zip(self.Stp[idx], stress_type[idx]), key=lambda x: x[0]))
        line4, = ax.plot(x,y,'-o',label="initial sepatation = 1.0")
        ax.set_xscale('log')
        plt.axvline(1.0,color='k', linestyle='--')
        plt.legend(handler_map={line1: HandlerLine2D(numpoints=4)},loc='upper left')
        plt.xlabel("Stokes number")
        plt.ylabel(stress_name)
        plt.title(stress_name +" vs stokes number for "+self.data_str)
        plt.savefig(stress_name+'_vs_StokesNumber_'+self.data_str, dpi=200)
        print "Generated shear_vs_stp plot for " + self.data_str
        #        plt.show()
        
    def shear_wrt_M_vs_Stp(self,stress_type,stress_name, M_val=[1,10,100,1000,10000]):
        if stress_type == 'snorm_tot': 
            stress_type = self.data.snorm_tot
        elif stress_type == 'snorm_hd': 
            stress_type = self.data.snorm_hd
        elif stress_type == 'snorm_lub': 
            stress_type = self.data.snorm_lub
        elif stress_type == 'snorm_rep': 
            stress_type = self.data.snorm_rep
        elif stress_type == 'sshear_hd': 
            stress_type = self.data.sshear_hd
        elif stress_type == 'sshear_lub': 
            stress_type = self.data.sshear_lub
        elif stress_type == 'sshear_rep': 
            stress_type = self.data.sshear_rep
        elif stress_type == 'sshear_tot': 
            stress_type = self.data.sshear_tot
        elif stress_type == 'f_hd': 
            stress_type = self.data.F_hd
        elif stress_type == 'f_lub': 
            stress_type = self.data.F_lub
        elif stress_type == 'f_rep': 
            stress_type = self.data.F_rep
        elif stress_type == 'f_tot': 
            stress_type = self.data.F_tot
        else:
            print "enter valid shear/strss name."
            
        for m in M_val:   
            try:
                fig = plt.figure()
                fig.set_size_inches(15.5, 5.5)
                ax = fig.add_subplot(1,1,1)
                idx = np.where((self.M == m)&(self.sep_ratio == 0.25))[0]
                [x, y] = zip(*sorted(zip(self.Stp[idx], stress_type[idx]), key=lambda x: x[0]))
                line1, = ax.plot(x,y,'-o',label="initial sepatation = 0.25")
                idx = np.where((self.M == m)&(self.sep_ratio == 0.5))[0]
                [x, y] = zip(*sorted(zip(self.Stp[idx], stress_type[idx]), key=lambda x: x[0]))
                line1, = ax.plot(x,y,'-o',label="initial sepatation = 0.5")
                idx = np.where((self.M == m)&(self.sep_ratio == 0.75))[0]
                [x, y] = zip(*sorted(zip(self.Stp[idx], stress_type[idx]), key=lambda x: x[0]))
                line1, = ax.plot(x,y,'-o',label="initial sepatation = 0.75")
                idx = np.where((self.M == m)&(self.sep_ratio == 1.0))[0]
                [x, y] = zip(*sorted(zip(self.Stp[idx], stress_type[idx]), key=lambda x: x[0]))
                line1, = ax.plot(x,y,'-o',label="initial sepatation = 1.0")
                ax.set_xscale('log')
                plt.axvline(1.0,color='k', linestyle='--')
                plt.legend(handler_map={line1: HandlerLine2D(numpoints=4)},loc='center left')
                plt.xlabel("Stokes number")
                plt.ylabel(stress_name)
                plt.title(stress_name +"  vs stokes number with M = "+str(m)+" for "+self.data_str)
                plt.savefig( stress_name+'_vs_StokesNumber_M_'+str(m)+"_"+self.data_str, dpi=200)
                print "Generated shear_vs_stp plot for M = "+str(m) + "for " + self.data_str
            except:
                print "trouble. check code"
                continue
            #           plt.show()
     
#normal stresses here      
shear_analysis = analysis("normal_stress_info.txt","total normal stress") 
shear_analysis.shear_wrt_M_vs_Stp('snorm_tot', "total normal stress") 
shear_analysis.plot_shear_vs_stp('snorm_tot', "total normal stress") 
shear_analysis.plot_shear_errorbar_vs_stp('snorm_tot'," mean of total normal stress")

shear_analysis = analysis("normal_stress_info.txt","total normal hd stress") 
shear_analysis.shear_wrt_M_vs_Stp('snorm_hd', "total normal hd stress") 
shear_analysis.plot_shear_vs_stp('snorm_hd', "total normal hd stress") 
shear_analysis.plot_shear_errorbar_vs_stp('snorm_hd', "mean of total normal hd stress")

shear_analysis = analysis("normal_stress_info.txt","total normal lubrication stress") 
shear_analysis.shear_wrt_M_vs_Stp('snorm_lub', "total normal lub stress") 
shear_analysis.plot_shear_vs_stp('snorm_lub', "total normal lub stress") 
shear_analysis.plot_shear_errorbar_vs_stp('snorm_lub', "mean of total normal lub stress")

shear_analysis = analysis("normal_stress_info.txt","total normal repulsive stress") 
shear_analysis.shear_wrt_M_vs_Stp('snorm_rep', "total normal repulsive stress") 
shear_analysis.plot_shear_vs_stp('snorm_rep', "total normal repulsive stress") 
shear_analysis.plot_shear_errorbar_vs_stp('snorm_rep', "mean of total normal repulsive stress")

#shear stresses here 
shear_analysis = analysis("shear_stress_info.txt","total shear stress") 
shear_analysis.shear_wrt_M_vs_Stp('sshear_tot', "total shear stress") 
shear_analysis.plot_shear_vs_stp('sshear_tot', "total shear stress") 
shear_analysis.plot_shear_errorbar_vs_stp('sshear_tot', "mean of total shear stress")

shear_analysis = analysis("shear_stress_info.txt","total shear hd stress") 
shear_analysis.shear_wrt_M_vs_Stp('sshear_hd', "total shear hd stress") 
shear_analysis.plot_shear_vs_stp('sshear_hd', "total shear hd stress") 
shear_analysis.plot_shear_errorbar_vs_stp('sshear_hd', "mean of total shear hd stress")

shear_analysis = analysis("shear_stress_info.txt","total shear lub stress") 
shear_analysis.shear_wrt_M_vs_Stp('sshear_lub', "total shear lub stress") 
shear_analysis.plot_shear_vs_stp('sshear_lub', "total shear lub stress") 
shear_analysis.plot_shear_errorbar_vs_stp('sshear_lub', "mean of total shear lub stress") 

shear_analysis = analysis("shear_stress_info.txt","total shear repulsive stress") 
shear_analysis.shear_wrt_M_vs_Stp('sshear_rep', "total shear repulsive stress") 
shear_analysis.plot_shear_vs_stp('sshear_rep', "total shear repulsive stress") 
shear_analysis.plot_shear_errorbar_vs_stp('sshear_rep', "mean of total shear repulsive stress") 

#Forces here 
shear_analysis = analysis("force_info.txt","total  force") 
shear_analysis.shear_wrt_M_vs_Stp('f_tot', "total  force") 
shear_analysis.plot_shear_vs_stp('f_tot', "total  force") 
shear_analysis.plot_shear_errorbar_vs_stp('f_tot', "mean of total  force") 

shear_analysis = analysis("force_info.txt","total hd force") 
shear_analysis.shear_wrt_M_vs_Stp('f_hd', "total hd force") 
shear_analysis.plot_shear_vs_stp('f_hd', "total hd force") 
shear_analysis.plot_shear_errorbar_vs_stp('f_hd', "mean of total hd force")

shear_analysis = analysis("force_info.txt","total lub force") 
shear_analysis.shear_wrt_M_vs_Stp('f_lub', "total lub force") 
shear_analysis.plot_shear_vs_stp('f_lub', "total lub force") 
shear_analysis.plot_shear_errorbar_vs_stp('f_lub', "mean of total lub force") 

shear_analysis = analysis("force_info.txt","total repulsive force") 
shear_analysis.shear_wrt_M_vs_Stp('f_rep', "total repulsive force") 
shear_analysis.plot_shear_vs_stp('f_rep', "total repulsive force") 
shear_analysis.plot_shear_errorbar_vs_stp('f_rep', "mean of total repulsive force") 
