# README #

This is a repository for the simulations that are being conducted using the susi code.

### What is this repository for? ###

* SuSi is a high-fidelity simulation tool for suspensions.

Suspensions are systems of a fluid (the solvent) and many particles suspended in it. Typically, the particles have a diameter of a few m. If the volume fraction (the percentage the particles take of the total volume) is medium to high suspensions show strong non-Newtonian effects, i.e. the response to external stresses like shear deformation is highly non-linear and might also depend on the history of deformation. Also, the transport of the particles in such an environment shows a lot of effects that cannot yet be described theoretically.

### How do I get set up? ###

* contact https://electricant.com/